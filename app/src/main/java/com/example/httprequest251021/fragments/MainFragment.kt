package com.example.httprequest251021.fragments

import android.content.ContentValues.TAG
import android.util.Log
import android.widget.Toast
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.httprequest251021.adapter.RecyclerAdapter
import com.example.httprequest251021.databinding.FragmentMainBinding
import com.example.httprequest251021.network.ResultOf


class MainFragment : BaseFragment<FragmentMainBinding>(FragmentMainBinding::inflate) {

    private val viewModel: MainViewModel by viewModels()

    private lateinit var adapter: RecyclerAdapter



    override fun start() {

        initRecycler()
        listeners()
        observes()
        viewModel.load()
        Log.d("result", "observes: ${viewModel.data.value}")

    }

    private fun initRecycler() {
        binding.recycler.layoutManager = LinearLayoutManager(requireContext())
        adapter = RecyclerAdapter()
        binding.recycler.adapter = adapter
    }

    private fun listeners() {
        binding.swipe.setOnRefreshListener {
            viewModel.load()
            Log.d("result", "observes: ${viewModel.data.value}")
        }
    }



    private fun observes() {
        binding.swipe.isRefreshing = true

        viewModel.data.observe(viewLifecycleOwner, {result ->

            when (result) {

                is ResultOf.Success -> {
                    adapter.setData(result.value!!)
                    binding.swipe.isRefreshing = false
//                    Log.d("result", "observes: ${result}")

                }

                is ResultOf.Failure -> {
                    Toast.makeText(
                        requireContext(), result.message
                            ?: "Unknown error", Toast.LENGTH_SHORT
                    ).show()
//                    Log.d("result", "observes: ${result}")
                    binding.swipe.isRefreshing = false
                }
            }
        })

    }


}