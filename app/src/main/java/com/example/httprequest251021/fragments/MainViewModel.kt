package com.example.httprequest251021.fragments


import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.httprequest251021.model.NewsItem
import com.example.httprequest251021.network.NetworkClient
import com.example.httprequest251021.network.ResultOf
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okio.IOException
import retrofit2.HttpException

class MainViewModel: ViewModel() {

    private val _data = MutableLiveData<ResultOf<MutableList<NewsItem>>>()
    val data: LiveData<ResultOf<MutableList<NewsItem>>> get() = _data


    fun load() {
        viewModelScope.launch {
            try {
                val response = NetworkClient.apiClient.getNews()
                val result = response.body()

                if (response.isSuccessful && result != null){
                    Log.d("result", "load:${result as MutableList<NewsItem>?}")
                    _data.postValue (ResultOf.Success(response as MutableList<NewsItem>))
//                    Log.d("result", "load:${result}")
//                    Log.d("result", "observes: ${_loading.value}")



                }

            } catch (ioe: IOException) {
                _data.postValue(ResultOf.Failure("[IO] error please retry", ioe))
            } catch (he: HttpException) {
                _data.postValue(ResultOf.Failure("[HTTP] error please retry", he))
            }


        }


        }
    }
























//    private suspend fun getNews() {
//        _loading.postValue(true)
//        try {
//            //konstrukcia(val result) gvibrunebs http requests
//            val result = NetworkClient.apiClient.getNews()
//            _loading.postValue(false)
//            val body = result.body()
//            if (result.isSuccessful && body != null) {
//
//            }else {
//
//            }
//        }catch (e:Exception){}
//    }
//
//}