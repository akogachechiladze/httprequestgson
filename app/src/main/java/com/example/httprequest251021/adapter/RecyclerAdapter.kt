package com.example.httprequest251021.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.httprequest251021.model.NewsItem
import com.example.httprequest251021.databinding.ItemsLayoutBinding
import com.example.httprequest251021.setImage

class RecyclerAdapter: RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

    private val news = mutableListOf<NewsItem>()



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        ItemsLayoutBinding.inflate(
            LayoutInflater.from(parent.context),parent,false
        )
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    override fun getItemCount()= news.size

    inner class ViewHolder(val binding: ItemsLayoutBinding): RecyclerView.ViewHolder(binding.root){
        private lateinit var model: NewsItem
        fun onBind() {
            model = news[adapterPosition]
            binding.IV1.setImage(model.imgUrl)
            binding.TV1.text = model.title.toString()
        }
    }

    fun setData(news: MutableList<NewsItem>) {
        news.clear()
        news.addAll(news)
        notifyDataSetChanged()

    }

}