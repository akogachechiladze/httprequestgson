package com.example.httprequest251021.model

import com.google.gson.annotations.SerializedName

data class NewsItem(
//        val created_at: Any,
//        val date: String,
//        val deleted_at: Any,
//        val description: String,
//        val id: Int,
//        val images: Any,
        @SerializedName("img-url")
        val imgUrl: String,
        val title: String,
//        val updated_at: Any
)